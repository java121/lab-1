# LAB 1


Background

An array is used to store a collection of data, but it is often considered that an array is a collection of variables of the same type. An array of elements of a primitive data type is an object created by a class Array of the package java.lang. Class java.util. Arrays is used for arrays whose elements are of any referenced data type. It contains methods for sorting, searching, comparing arrays, and filling array elements. Once the array object is created, its size is fixed in the memory. 

1.	Define an array

Consider the following statements:

`int[] listA = {5, 10, 15, 20, 25, 30, 35}; // Line 1
int[] listB = new int[listA.length];       // Line 2`

The statement in Line 1 creates the array listA of size 7 and also initializes the array. In Line 2, an array listB is created by using the length of the array listA that is the number of elements of the array. The size of an array cannot be changed after the array is created. 
Note that the value of listA.length is 7. The elements’ values of listB are 0s.

2.	Arrays as Parameters to Methods

Just like other objects, arrays can be passed as parameters to methods. The following method takes as an argument any int array and outputs the data stored in each element:

`public static void printArray(int[] list) 
{ 
for (int index = 0; index < list.length; index++) System.out.print(list[index] + " "); 
}`


3.	Copying  Arrays

There are three ways to copy arrays:
•	Use a loop to copy individual elements one by one.
•	Use the static arraycopy() method  in the System class.
•	Use the clone() method.

`System.arraycopy(sourceArray, sPos, targetArray , tPos, length);`


  sPos, tPos – are starting positions of the source and target arrays
  sourceArray – the array whose elements will be copied 
  targetArray – the array where the elements will be copied
  length – the number of elements copied.


Note: In  order  to  use  this method  to copy arrays , you have to create the  source and  target arrays previously .  

Exercises: 

1.	What is the output of the following program segment? 

`int[] temp = new int[5];
for (int i = 0; i < 5; i++) 
temp[i] = 2 * i - 3;
for (int i = 0; i < 5; i++) 
System.out.print(temp[i] + " "); 
System.out.println();
temp[0] = temp[4];
 temp[4] = temp[1]; 
temp[2] = temp[3] + temp[0];
for (int i = 0; i < 5; i++) 
System.out.print(temp[i] + " "); 
System.out.println();
`

2.	Correct the following code so that it correctly initializes and outputs the elements of the array myList: 

`Scanner console = new Scanner(System.in);
int[] myList = new[10];
for (int i = 1; i <= 10; i++) myList = console.nextInt();
for (int i = 1; i <= 10; i++) System.out.print(myList[i] + " "); System.out.println();`


Note: the Scanner class is used to enter data to your program from the standard input devise. Using this class, we first create an input stream object and associate it with the standard input device. Scanner class is available in the java package java.util.


3.	Copy an array from another one. What is the output of the code below:
` public static void main(String[] args) {
        char[] copyFrom = { 'd', 'e', 'c', 'a', 'f', 'f', 'e',
			    'i', 'n', 'a', 't', 'e', 'd' };
        char[] copyTo = new char[7];

        System.arraycopy(copyFrom, 2, copyTo, 0, 7);
        System.out.println(new String(copyTo));
       }`
…………………………………………………………………………………………………………….

The next program code uses the method clone(). Copy the code and run the program to obtain the result.

`import java.util.Arrays; 
 
public class ArrayCopyApp { 
 
public static void main(String[] args) {         
float[] floatArray = {5.0f, 3.0f, 2.0f, 1.5f};        
float[] floatArrayCopy =(float[]) floatArray.clone();         
System.out.println(Arrays.toString(floatArray) + " - Original");
System.out.println(Arrays.toString(floatArrayCopy) + " - Copy");
System.out.println();         
System.out.println("Modifying the second element of the original array");         
floatArray[1] = 20;         
System.out.println(Arrays.toString(floatArray) + " - Original after modification");         
System.out.println(Arrays.toString(floatArrayCopy) + " - Copy");        
System.out.println();         
System.out.println("Modifying the third element of the copy array");         
floatArrayCopy[2] = 30;         
System.out.println(Arrays.toString(floatArray) + " - Original");
System.out.println(Arrays.toString(floatArrayCopy) + " - Copy array after modification");     
} 
}    `


The clone() method copies all the elements of the floatArray into a new array called floatArrayCopy. To confirm that a new copy is  made, we will try modifying an element of each of the two arrays, printing both each time. To print the contents of an array, we use the expression Arrays.toString(floatArray). 
The toString() method of this class takes an argument of the array type and converts its contents to a string. Another approach to print the elements of an array would be to use a traditional loop to iterate through all the array elements, printing each in the iteration cycle. The program first modifies the element at index 1 of the original array and then it modifies the element at index 2 of the copied array. Each time, both arrays are printed after modifications.

POSTLAB Exercises 

1.	Write  a program that defines an array of  integer  type . The number of elements is 10  and  their values are random integers between 1 and 100 . The program has to process the array  in order to find  the index  of the element with  the smallest value . The program should print out the smallest value and  its index  of the last occurrence .
Hint : To provide  random  integers  between 1  and  100  use  the  formula :
                      (int)( 1 + Math.random() * 100) 
2.	Write a Java program that declares an int array and its size. Enter the elements’ values randomly. The program returns the sum of all odd integers and the sum of all even integers.

